import argparse
import network
import numpy as np
import torch
import json
from PIL import Image
from torchvision import datasets, transforms, models

def get_args():

    parser = argparse.ArgumentParser(description="Train FLower Cl")
    parser.add_argument('image', type=str, help="input image")
    parser.add_argument('checkpoint', type=str, help="model path")
    parser.add_argument('--topk', default=5, help='number of top results')
    parser.add_argument('--cat_names', default='', type=str, help='mapping file')
    return parser.parse_args()

def predict(image, model, k=5):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    
    # TODO: Implement the code to predict the class from an image file
    #model = load_model(model_path)
    #model.to('cpu')
    
    #image_transformed = process_image(image_path)
    
   
    #image_transformed = image_transformed.unsqueeze_(0)
    
    #image_transformed = image_transformed.to('cuda')
        

    result = model.forward(image)
    probs = torch.exp(result)
        
    topk_probs, topk_class = probs.topk(k)
    
    idx_to_class ={}
    
    for idx, item in model.class_to_idx.items():
        idx_to_class[item] = idx
    
    #topk_class.to('cpu')
    topk_class_np = topk_class[0].numpy()
    
    top_classes = []
    for clas in topk_class_np:
        top_classes.append(int(idx_to_class[clas]))
                
    #top = [cat_to_name[str(clas)] for clas in top_classes]
       
    return topk_probs, topk_class, top_classes

def process_image(image_path):
        # TODO: Process a PIL image for use in a PyTorch model
    ''' Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    '''
    i = Image.open(image_path)
    
    transform = transforms.Compose([transforms.Resize(256),
                                    transforms.CenterCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])                                                                       
                                   ])
    i_transformed = transform(i)
    
        
    return i_transformed


def main():
    args = get_args()
    image_transformed = process_image(args.image)
    image_transformed = image_transformed.unsqueeze_(0)
    model = network.load_model(args.checkpoint)
    probs, class_top, top = predict(image_transformed, model, args.topk)
    
    print(f" Predicitions: {list(zip(top, probs))}")
    
if __name__ == '__main__':
    main()