import torch
from torchvision import datasets, transforms

def get_DataLoaders(data_dir = "ImageClassifier/flowers"):
    train_dir = data_dir + '/train'
    valid_dir = data_dir + '/valid'
    test_dir = data_dir + '/test'
    
    data_transforms_train = transforms.Compose([transforms.RandomRotation(31),
                                            transforms.RandomResizedCrop(224),
                                            transforms.RandomHorizontalFlip(),
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

    data_transforms_test = transforms.Compose([transforms.RandomResizedCrop(224),
                                               transforms.ToTensor(),
                                               transforms.Normalize([0.485, 0.456, 0.406],[0.229, 0.224, 0.225])])


    
    image_train = datasets.ImageFolder(data_dir + '/train', transform = data_transforms_train)

    image_valid = datasets.ImageFolder(data_dir + '/valid', transform = data_transforms_test)

    image_test = datasets.ImageFolder(data_dir + '/test', transform = data_transforms_test)

    trainloader = torch.utils.data.DataLoader(image_train, batch_size = 64, shuffle=True)
    validloader = torch.utils.data.DataLoader(image_valid, batch_size = 32)
    testloader = torch.utils.data.DataLoader(image_test, batch_size = 32)
    class_to_idx = image_train.class_to_idx
    return trainloader, validloader, testloader, class_to_idx