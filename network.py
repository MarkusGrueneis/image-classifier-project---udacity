from torchvision import datasets, transforms, models
from torch import nn, optim
from torch.autograd import Variable
import json
import numpy as np
import torch
from collections import OrderedDict
from PIL import Image

def build_network(arch='vgg19'):
    if arch == 'vgg19':
        print("Pretrained vgg19")
        model = models.vgg19(pretrained=True)
    else:
        print("model not available")
        print("Pretrained vgg19")
        model = models.vgg19(pretrained=True)
    
    for param in model.parameters():
        param.requires_grad = False
    
    classifier = nn.Sequential(OrderedDict([('fc1',
                                             nn.Linear(25088, 1024)),
                                            ('relu', nn.ReLU()),
                                            ('dropout', nn.Dropout(0.3)),
                                            ('fc2', nn.Linear(1024, 102)),
                                            ('output', nn.LogSoftmax(dim=1))]))

    model.classifier = classifier
    return model

def get_loss_function():
    return nn.NLLLoss()

def get_optimizer(model, lr):
    return torch.optim.Adam(model.classifier.parameters(), lr)

def save_model(model, arch, epochs, lr, hidden_units):
    save_path = f'./checkpoint.pth'
    
    #model.class_to_idx = image_train.class_to_idx
    model.cpu()
    
    checkpoint = {'input_size': 25088,
              'output_size': 102,
              'arch': 'vgg19',
              'classifier': model.classifier,
              'state_dict': model.state_dict(),
              'mapping': model.class_to_idx
             }
    
    torch.save(checkpoint, save_path)

def load_model(file_path):
    checkpoint = torch.load(file_path)
    model = models.vgg19(pretrained = True)
    
    model.classifier = checkpoint['classifier']
    model.load_state_dict(checkpoint['state_dict'])
    model.class_to_idx = checkpoint['mapping']
    
    return model

