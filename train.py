import argparse
import network
import data_loader
import torch
from torch.autograd import Variable

def get_args():
    parser = argparse.ArgumentParser(description="Input Parameters for FLower Classification Model")
    parser.add_argument('data_dir', default='/flowers/train', type=str, help="directory")
    parser.add_argument('--save_dir', default='', type=str, help="directory to save checkpoints")
    parser.add_argument('--arch', default='vgg19', help='pretrained network architecture, currently only vgg19 available (default).')
    parser.add_argument('--learning_rate', default=0.001, type=float, help='learning rate (default=0.001)')
    parser.add_argument('--hidden_units', default=1024, type=int, help='number of hidden layer units')
    parser.add_argument('--dropout', default=0.3, type=float, help='dropout probability for training')
    parser.add_argument('--epochs', default=3, type=int, help='number of training epochs')
    
def train(model, epochs, optimizer, criterion, trainloader, validloader):
    
    print_every = 50
    steps = 0
    
    model.train()
    
    if torch.cuda.is_available():
        gpu = True
        model.cuda()
    else:
        gpu = False
        model.cpu()
    
    for epoch in range(epochs):

        running_loss = 0    
        
        for images, labels in iter(trainloader):
            steps+=1
            
            if gpu:
                inputs = Variable(images.float().cuda())
                labels = torch.tensor(labels)
                labels = Variable(labels.long().cuda())
            #else:
             #   inputs = Variable(images)
              #  labels = labels
            
            optimizer.zero_grad()
        
            logps = model.forward(inputs)
            loss = criterion(logps, labels)
            loss.backward()
            optimizer.step()
        
            running_loss += loss.item()
        
            if steps % print_every == 0:
                valid_loss, accuracy = validation(model, criterion, validloader, gpu)
            
                print(f"Epoch {epoch+1}/{epochs}____.____ "
                      f"Train loss: {running_loss/print_every:.3f}____.____ "
                      f"Valid loss: {valid_loss:.3f}____.____ "
                      f"Valid accuracy: {accuracy:.3f}")

def validation(model, criterion, validloader, gpu):
    
    valid_loss = 0
    accuracy = 0
    
    for images, labels in iter(validloader):
        with torch.no_grad():
                    
            if gpu:
                inputs = Variable(images.float().cuda())
                labels = Variable(labels.long().cuda())
        
            else:
                inputs = Variable(images)
                labels = Variable(labels)
            
            logps = model.forward(inputs)
            loss = criterion(logps, labels)
            valid_loss += loss.item()
        
            ps = torch.exp(logps).data 
            equal = (labels.data == ps.max(1)[1])
            accuracy += equal.type_as(torch.FloatTensor()).mean()
    
    loss = valid_loss/len(validloader)
    acc = accuracy/len(validloader)
    
    return loss, acc         
   
def main():
    args = get_args()
    #print_training_config(args)
    trainloader, validloader, testloader, class_to_idx = data_loader.get_DataLoaders()
    model = network.build_network()
    model.class_to_idx = class_to_idx
    criterion = network.get_loss_function()
    optimizer = network.get_optimizer(model, 0.001)
    train(model, 1, optimizer, criterion, trainloader, validloader)
    network.save_model(model, 'vgg19', 3, 0.001, 1024)
    
#def print_training_config(args):

if __name__ == '__main__':
    main()
    

        
    
                        